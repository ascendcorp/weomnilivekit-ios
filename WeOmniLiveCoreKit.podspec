#
# Be sure to run `pod lib lint WeOmniLiveCoreKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'WeOmniLiveCoreKit'
  s.version          = '0.1.0'
  s.summary          = 'WeOmniLiveCoreKit'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  Live Commerce SDK for iOS
                       DESC

  s.homepage         = 'https://bitbucket.org/ascendcorp/weomnilivekit-ios.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Chayanon Ardkham' => 'chayanon.ard@ascendcorp.com' }
  s.source           = { :git => 'https://bitbucket.org/ascendcorp/weomnilivekit-ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'
  s.swift_version = '5'

  s.source_files = 'WeOmniLiveCoreKit/Classes/**/*'
  
  s.resource_bundles = {
    'WeOmniLiveCoreKit' => ['WeOmniLiveCoreKit/Classes/**/*.{xib,json}']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'Moya', '~> 14.0'
  s.dependency 'ObjectMapper', '~> 3.5'
  s.dependency 'SwiftLint'
end
