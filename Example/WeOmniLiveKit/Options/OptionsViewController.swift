//
//  OptionsViewController.swift
//  WeOmniLiveKit_Example
//
//  Created by Chayanon Ardkham on 18/3/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import WeOmniLiveCoreKit

final class OptionsViewController: UITableViewController {
    
    // MARK: MODEL
    var isChanged: Bool = false {
        didSet {
            revertButton.isEnabled = isChanged
            confirmButton.isEnabled = isChanged
        }
    }
    
    var configuration = WLKConfiguration(clientId: "", clientSecret: "", projectId: "", environment: .mock)
    
    // MARK: VIEW
    @IBOutlet weak var revertButton: UIBarButtonItem!
    @IBOutlet weak var confirmButton: UIBarButtonItem!
    
    @IBOutlet weak var projectIdTextField: UITextField!
    @IBOutlet weak var clientIdTextField: UITextField!
    @IBOutlet weak var clientSecretTextField: UITextField!
    @IBOutlet weak var environmentSelectorButton: UIButton!
    
    // MARK: VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        
        loadSavedConfiguration()
    }
    
    func loadSavedConfiguration() {
        configuration = AppVariables.appVariables().configuration
        updateOptionsViewController()
    }
    
    func updateOptionsViewController() {
        projectIdTextField.text = configuration.projectId
        clientIdTextField.text = configuration.clientId
        clientSecretTextField.text = configuration.clientSecret
        environmentSelectorButton.setTitle(configuration.environment.rawValue, for: .normal)
    }
    
    // MARK: ACTIONS
    @IBAction func didProjectIdTextFieldEditingChanged(_ sender: Any) {
        configuration.projectId = projectIdTextField.text ?? ""
        isChanged = true
    }
    
    @IBAction func didClientIdTextFieldEditingChanged(_ sender: Any) {
        configuration.clientId = clientIdTextField.text ?? ""
        isChanged = true
    }
    
    @IBAction func didClientSecretTextFieldEditingChanged(_ sender: Any) {
        configuration.clientSecret = clientSecretTextField.text ?? ""
        isChanged = true
    }
    
    @IBAction func didTapOnEnvironmentSelectorButton(_ sender: Any) {
        let alertController = UIAlertController(title: "Environment Selector", message: nil, preferredStyle: .actionSheet)
        
        let mockAction = UIAlertAction(title: "Mock", style: .default) { (_) in
            self.configuration.environment = .mock
            self.isChanged = true
            self.updateOptionsViewController()
        }
        alertController.addAction(mockAction)
        
        let alphaAction = UIAlertAction(title: "Alpha", style: .default) { (_) in
            self.configuration.environment = .alpha
            self.isChanged = true
            self.updateOptionsViewController()
        }
        alertController.addAction(alphaAction)
        
        let stagingAction = UIAlertAction(title: "Staging", style: .default) { (_) in
            self.configuration.environment = .staging
            self.isChanged = true
            self.updateOptionsViewController()
        }
        alertController.addAction(stagingAction)
        
        let productionAction = UIAlertAction(title: "Production", style: .default) { (_) in
            self.configuration.environment = .production
            self.isChanged = true
            self.updateOptionsViewController()
        }
        alertController.addAction(productionAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func didTapOnRevertButton(_ sender: Any) {
        loadSavedConfiguration()
        isChanged = false
    }
    
    @IBAction func didTapOnConfirmButton(_ sender: Any) {
        AppVariables.appVariables().configuration = configuration
        isChanged = false
    }
}
