//
//  AppVariables.swift
//  WeOmniLiveKit_Example
//
//  Created by Chayanon Ardkham on 18/3/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import WeOmniLiveCoreKit

class AppVariables {
    
    // MARK: INSTANCE
    private init() {}
    private static let shared = AppVariables()
    
    public static func appVariables() -> AppVariables {
        return shared
    }
    
    // MARK: VARIABLES
    private var _wlk: WeOmniLiveKit!
    var wlk: WeOmniLiveKit {
        _wlk = _wlk ?? WeOmniLiveKit(configuration: configuration)
        return _wlk
    }
    
    var configuration: WLKConfiguration {
        get {
            let configuration = WLKConfiguration(clientId: "", clientSecret: "", projectId: "", environment: .mock)
            if let projectId = UserDefaults.standard.value(forKey: "WeOmniLiveKit.configuration.projectId") as? String {
                configuration.projectId = projectId
            }
            
            if let clientId = UserDefaults.standard.value(forKey: "WeOmniLiveKit.configuration.clientId") as? String {
                configuration.clientId = clientId
            }
            
            if let clientSecret = UserDefaults.standard.value(forKey: "WeOmniLiveKit.configuration.clientSecret") as? String {
                configuration.clientSecret = clientSecret
            }
            
            if let environment = UserDefaults.standard.value(forKey: "WeOmniLiveKit.configuration.environment") as? String {
                configuration.environment = WLKConfiguration.Environment(rawValue: environment)
            }
            
            return configuration
        }
        set {
            UserDefaults.standard.setValue(newValue.projectId, forKey: "WeOmniLiveKit.configuration.projectId")
            UserDefaults.standard.setValue(newValue.clientId, forKey: "WeOmniLiveKit.configuration.clientId")
            UserDefaults.standard.setValue(newValue.clientSecret, forKey: "WeOmniLiveKit.configuration.clientSecret")
            UserDefaults.standard.setValue(newValue.environment.rawValue, forKey: "WeOmniLiveKit.configuration.environment")
        }
    }
}
