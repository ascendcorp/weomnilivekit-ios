//
//  AppDelegate.swift
//  WeOmniLive
//
//  Created by Chayanon Ardkham on 1/3/21.
//

import UIKit
import WeOmniLiveCoreKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppVariables.appVariables().wlk.application(application, didFinishLaunchingWithOptions: launchOptions)
        FirebaseApp.configure()
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppVariables.appVariables().wlk.applicationDidBecomeActive(application)
    }
}
