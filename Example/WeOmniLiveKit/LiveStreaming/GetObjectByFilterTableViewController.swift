//
//  GetObjectByFilterTableViewController.swift
//  WeOmniLiveKit_Example
//
//  Created by Chayanon Ardkham on 22/3/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import WeOmniLiveCoreKit

// MARK: CLASS
final class GetObjectByFilterTableViewController: UITableViewController {
    
    // MARK: MODEL
    private var filter: ([WLKStreamingRoom.Status], Int, Int, [String: Any]) {
        // Status
        var status = [WLKStreamingRoom.Status]()
        unpublishedSwitch.isOn ? status.append(.unpublished) : ()
        liveNowSwitch.isOn ? status.append(.live) : ()
        processingSwitch.isOn ? status.append(.processing) : ()
        rerunSwitch.isOn ? status.append(.rerun) : ()
        liveEndedSwitch.isOn ? status.append(.ended) : ()
        bannedSwitch.isOn ? status.append(.banned) : ()
        
        // Size
        let size = Int(numberOfItemPerPageTextField.text ?? "") ?? 20
        
        // Page
        let page = Int(pageNumberTextField.text ?? "") ?? 0
        
        // Custom Parameters
        var customParameters = [String: Any]()
        for (index, refTextfield) in refTextFields.enumerated() where !(refTextfield.text?.isEmpty ?? false) {
            customParameters["ref\(index + 1)"] = refTextfield.text!
        }
        
        return (status, size, page, customParameters)
    }
    
    private var refTextFields: [UITextField] {
        return [ref1TextField, ref2TextField, ref3TextField, ref4TextField, ref5TextField, ref6TextField, ref7TextField, ref8TextField, ref9TextField, ref10TextField]
    }
    
    // MARK: VIEW
    @IBOutlet private weak var unpublishedSwitch: UISwitch!
    @IBOutlet private weak var liveNowSwitch: UISwitch!
    @IBOutlet private weak var processingSwitch: UISwitch!
    @IBOutlet private weak var rerunSwitch: UISwitch!
    @IBOutlet private weak var liveEndedSwitch: UISwitch!
    @IBOutlet private weak var bannedSwitch: UISwitch!
    
    @IBOutlet private weak var numberOfItemPerPageTextField: UITextField!
    @IBOutlet private weak var pageNumberTextField: UITextField!
    
    @IBOutlet private weak var ref1TextField: UITextField!
    @IBOutlet private weak var ref2TextField: UITextField!
    @IBOutlet private weak var ref3TextField: UITextField!
    @IBOutlet private weak var ref4TextField: UITextField!
    @IBOutlet private weak var ref5TextField: UITextField!
    @IBOutlet private weak var ref6TextField: UITextField!
    @IBOutlet private weak var ref7TextField: UITextField!
    @IBOutlet private weak var ref8TextField: UITextField!
    @IBOutlet private weak var ref9TextField: UITextField!
    @IBOutlet private weak var ref10TextField: UITextField!
    
    // MARK: VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToResultView", let viewController = segue.destination as? GetObjectByFilterResultViewController {
            viewController.filter = filter
        }
    }
}
