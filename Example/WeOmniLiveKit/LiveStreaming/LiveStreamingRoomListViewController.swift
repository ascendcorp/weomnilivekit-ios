//
//  LiveStreamingRoomListViewController.swift
//  WeOmniLive
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import UIKit
import WeOmniLiveCoreKit
import WeOmniLiveBuyerKit

// MARK: - CLASS
final class LiveStreamingRoomListViewController: UIViewController {
    
    // MARK: MODEL
    var roomListView: WLKStreamingRoomListView?
    
    // MARK: VIEW
    @IBOutlet weak var viewWidthTextField: UITextField!
    @IBOutlet weak var viewHeightTextField: UITextField!
    @IBOutlet weak var scrollDirectionSelectorSegmentedControl: UISegmentedControl!
    @IBOutlet weak var previewContainerView: UIView!
    
    // MARK: VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        previewContainerView.backgroundColor = .systemGreen
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if roomListView == nil {
            updateView()
        }
    }
    
    // MARK: ACTIONS
    @IBAction func didScrollDirectionSelectorSegmentedControlValueChanged(_ sender: Any) {
        updateView()
    }
    
    @IBAction func didViewWidthTextFieldEditingEnded(_ sender: Any) {
        updateView()
    }
    
    @IBAction func didViewHeightTextFieldEditingEnded(_ sender: Any) {
        updateView()
    }
    
    // MARK: FUNCTIONS
    private func updateView() {
        if let widthInt = Int(viewWidthTextField.text ?? ""),
           let heightInt = Int(viewHeightTextField.text ?? "") {
            
            let width = CGFloat(widthInt)
            let height = CGFloat(heightInt)
            let scrollDirection: UICollectionView.ScrollDirection = scrollDirectionSelectorSegmentedControl.selectedSegmentIndex == 0 ? .horizontal : .vertical
            
            generateRoomListView(width: width, height: height, scrollDirection: scrollDirection)
        }
    }
    
    private func generateRoomListView(width: CGFloat, height: CGFloat, scrollDirection: UICollectionView.ScrollDirection) {
        
        roomListView?.removeFromSuperview()
        roomListView = nil
        
        roomListView = WLKStreamingRoomListView(wlk: AppVariables.appVariables().wlk, scrollDirection: scrollDirection, status: WLKStreamingRoom.Status.allCases)
        
        if let roomListView = roomListView {
            
            // Set StreamingRoomListView's data
            roomListView.delegate = self
            
            let frame = CGRect(x: previewContainerView.frame.width / 2 - width / 2, y: previewContainerView.frame.height / 2 - height / 2, width: width, height: height)
            roomListView.frame = frame
            previewContainerView.addSubview(roomListView)
        }
    }
}

// MARK: - WLK STREAMINGROOMLISTVIEW DELEGATE
extension LiveStreamingRoomListViewController: WLKStreamingRoomListViewDelegate {
    
    func wlkStreamingRoomListView(_ wlkStreamingRoomListView: WLKStreamingRoomListView, didSelectStreamingRoom streamingRoom: WLKStreamingRoom) {
        let viewController = WLKStreamingViewController(wlk: AppVariables.appVariables().wlk,
                                                        streamingRoom: streamingRoom,
                                                        userId: "555",
                                                        displayName: "athena_test")
        viewController.delegate = self
        navigationController?.pushViewController(viewController, animated: true)
    }}

// MARK: - WLK STREAMINGVIEWCONTROLLER DELEGATE
extension LiveStreamingRoomListViewController: WLKStreamingViewControllerDelegate {
    func wlkStreamingViewController(_ viewController: WLKStreamingViewController, didSelectUrl url: String) {
        let alert = UIAlertController(title: "Return url", message: url, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
}
