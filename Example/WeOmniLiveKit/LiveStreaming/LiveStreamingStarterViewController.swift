//
//  LiveStreamingStarterViewController.swift
//  WeOmniLive
//
//  Created by Chayanon Ardkham on 2/3/21.
//

import Foundation
import UIKit
import WeOmniLiveCoreKit
import WeOmniLiveBuyerKit

final class LiveStreamingStarterViewController: UIViewController {
    
    @IBOutlet private weak var overLayWebViewUrlTextView: UITextField!
    @IBOutlet private weak var mediaUrlTextView: UITextField!
    @IBOutlet private weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        startButton.layer.cornerRadius = 8
    }
    
    @IBAction func didTapOnStartButton(_ sender: Any) {
        if let mediaUrlString = mediaUrlTextView.text {
            let streamingRoom = WLKStreamingRoom(JSON: ["title": "Manually Provided URL", "hlsUrl": mediaUrlString])!
            
            let viewController = WLKStreamingViewController(wlk: AppVariables.appVariables().wlk,
                                                            streamingRoom: streamingRoom,
                                                            overlayWebViewUrlString: overLayWebViewUrlTextView.text,
                                                            userId: "555",
                                                            displayName: "athena_test")
            viewController.delegate = self
            
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension LiveStreamingStarterViewController: WLKStreamingViewControllerDelegate {
    func wlkStreamingViewController(_ viewController: WLKStreamingViewController, didSelectUrl url: String) {
        let alert = UIAlertController(title: "Return url", message: url, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
}
