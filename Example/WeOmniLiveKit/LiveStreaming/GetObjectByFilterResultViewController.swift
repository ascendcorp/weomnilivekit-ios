//
//  GetObjectByFilterResultViewController.swift
//  WeOmniLiveKit_Example
//
//  Created by Chayanon Ardkham on 22/3/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import WeOmniLiveCoreKit
import WeOmniLiveBuyerKit

// MARK: - CLASS
final class GetObjectByFilterResultViewController: UIViewController {
    
    // MARK: MODEL
    public var filter: ([WLKStreamingRoom.Status], Int, Int, [String: Any])!
    
    private var roomListView: WLKStreamingRoomListView?
    private var streamingRooms = [WLKStreamingRoom]()
    
    // MARK: VIEW
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var rawResultTextView: UITextView!
    @IBOutlet weak var resultTypeSelectorSegmentedControl: UISegmentedControl!
    
    // MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let (status, size, page, customParameters) = filter
        AppVariables.appVariables().wlk.getStreamingRoomList(status: status, size: size, page: page, customParameters: customParameters) { (streamingRooms) in
            self.streamingRooms = streamingRooms
            self.updateView()
        } failure: { (error) in
            self.updateView(error: error)
        }
    }
    
    private func updateView(error: WLKError? = nil) {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.isHidden = true
        
        // RoomListView
        roomListView?.removeFromSuperview()
        roomListView = nil
        
        if let error = error {
            resultTypeSelectorSegmentedControl.selectedSegmentIndex = 1
            rawResultTextView.text = error.toJSONString(prettyPrint: true)
        } else {
            // RoomListView
            roomListView = WLKStreamingRoomListView(wlk: AppVariables.appVariables().wlk, scrollDirection: .vertical, streamingRooms: streamingRooms)
            if let roomListView = roomListView {
                
                // Set StreamingRoomListView's data
                roomListView.frame = view.frame
                view.addSubview(roomListView)
            }
            
            // Raw
            rawResultTextView.text = streamingRooms.toJSONString(prettyPrint: true)
        }
    }
    
    // MARK: ACTIONS
    @IBAction private func didResultTypeSelectorSegmentedControlValueChanged(_ sender: Any) {
        let shouldHideRoomListView = resultTypeSelectorSegmentedControl.selectedSegmentIndex == 1
        roomListView?.isHidden = shouldHideRoomListView
    }
}
