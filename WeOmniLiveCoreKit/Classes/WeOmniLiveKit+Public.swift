//
//  WeOmniLiveKit+Public.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 19/3/21.
//

import Foundation

extension WeOmniLiveKit {
    /// API-provided array of WLKStreamingRoom objects based on current configurations.
    public func getStreamingRoomList(status: [WLKStreamingRoom.Status], size: Int = 20, page: Int = 0, customParameters: [String: Any] = [String: Any](), success: @escaping ([WLKStreamingRoom]) -> Void, failure: @escaping (WLKError) -> Void) {
        service.roomService().requestObjects(type: WLKStreamingRoom.self, route: .getRoomList(status: status, size: size, page: page, customParameters: customParameters), maxAttempts: 1) { (streamingRooms) in
            success(streamingRooms)
        } error: { (error) in
            failure(error)
        }
    }
    public func getStreamingRoom(id: String, success: @escaping (WLKStreamingRoom) -> Void, failure: @escaping (WLKError) -> Void) {
        service.roomService().requestObject(type: WLKStreamingRoom.self, route: .getRoom(id: id), maxAttempts: 1) { (streamingRoom) in
            success(streamingRoom)
        } error: { (error) in
            failure(error)
        }
    }
}
