//
//  +UIApplication.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 15/3/21.
//

import Foundation
import UIKit

extension UIApplication {
    
    public class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    public func addSubViewToWindow(view: UIView) {
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(view)
    }
}
