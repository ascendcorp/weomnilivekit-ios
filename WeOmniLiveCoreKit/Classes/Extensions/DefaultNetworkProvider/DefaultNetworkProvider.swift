//
//  DefaultNetworkProvider.swift
//  KyuNetworkExtensions
//
//  Created by Chayanon Ardkham on 6/10/20.
//
//  swiftlint:disable force_cast

import Foundation
import Moya
import ObjectMapper

// MARK: - CLASS
public class DefaultNetworkProvider<T: TargetType, U: DefaultNetworkError> {
    
    // MARK: PRIVATE MODEL
    private weak var datasource: DefaultNetworkProviderDatasource?
    private var provider: MoyaProvider<T>!
    private var configuration: DefaultNetworkProviderConfiguration!
    
    // MARK: INITIALIZATION
    private init() {}
    public convenience init(configuration: DefaultNetworkProviderConfiguration, datasource: DefaultNetworkProviderDatasource? = nil) {
        self.init()
        
        let stubClosure: MoyaProvider<T>.StubClosure = { _ in
            return configuration.stubBehavior
        }
        
        let endpointClosure = { (target: T) -> Endpoint in
            let url = URL(target: target).absoluteString
            return Endpoint(url: configuration.baseUrlString + url, sampleResponseClosure: {.networkResponse(200, target.sampleData)}, method: target.method, task: target.task, httpHeaderFields: target.headers)
        }
        
        self.provider = MoyaProvider<T>(endpointClosure: endpointClosure, stubClosure: stubClosure, plugins: configuration.plugins)
        self.datasource = datasource
    }
    
    // MARK: PROVIDED PUBLIC FUNCTIONS
    /// Perform request without object mapping.
    public func requestPlain(route: T, success: @escaping (Response) -> Void, error: @escaping (U) -> Void) {
        if let datasource = datasource {
            datasource.prerequisiteProcessesForRequest(for: self) {
                self.performRequest(route: route, success: success, error: error)
            }
        } else {
            performRequest(route: route, success: success, error: error)
        }
    }
    
    /// Perform request and map response's data to specific object type.
    public func requestObject<O: BaseMappable>(type: O.Type, nestedAt path: String? = nil, route: T, maxAttempts: Int = 0, success: @escaping (O) -> Void, error: @escaping (U) -> Void) {
        request(isRequestingArrayOfObjects: false, type: type, nestedAt: path, route: route, maxAttempts: maxAttempts, success: { (object) in
            success(object as! O)
        }, error: error)
    }
    
    /// Perform request and map response's data to specific object type in array form.
    public func requestObjects<O: BaseMappable>(type: O.Type, nestedAt path: String? = nil, route: T, maxAttempts: Int = 0, success: @escaping ([O]) -> Void, error: @escaping (U) -> Void) {
        request(isRequestingArrayOfObjects: true, type: type, nestedAt: path, route: route, maxAttempts: maxAttempts, success: { (objects) in
            success(objects as! [O])
        }, error: error)
    }
}

// MARK: - PRIVATE MECHANISM
private extension DefaultNetworkProvider {
    
    /// Perform request and auto-map to specific object type.
    private func request<O: BaseMappable>(isRequestingArrayOfObjects: Bool, type: O.Type, nestedAt path: String?, route: T, maxAttempts: Int, success: @escaping (Any) -> Void, error: @escaping (U) -> Void) {
        requestPlain(route: route, success: { (response) in
            // Object(s) mapping
            if let serializedObject = Mapper<O>().map(jsonData: response.data, nestedAt: path) {
                success(serializedObject)
                return
            } else if isRequestingArrayOfObjects, let serializedObjects = Mapper<O>().mapArray(jsonData: response.data, nestedAt: path) {
                success(serializedObjects)
                return
            }
            
            // Error Handling
            self.requestErrorHandling(errorData: U(response: response), isRequestingArrayOfObjects: isRequestingArrayOfObjects, type: type, nestedAt: path, route: route, maxAttempts: maxAttempts - 1, success: success, error: error)
        }, error: { (errorData) in
            
            // Error Handling
            self.requestErrorHandling(errorData: errorData, isRequestingArrayOfObjects: isRequestingArrayOfObjects, type: type, nestedAt: path, route: route, maxAttempts: maxAttempts - 1, success: success, error: error)
        })
    }
    
    /// Perform Moya request.
    private func performRequest(route: T, success: @escaping (Response) -> Void, error: @escaping (U) -> Void) {
        self.provider.request(route) { (result) in
            switch result {
            case let .success(response):
                if (200..<300).contains(response.statusCode) {
                    success(response)
                } else {
                    // Try to map error object from current response's data.
                    if let serializedObject = Mapper<U>().map(jsonData: response.data) {
                        serializedObject.response = response
                        error(serializedObject)
                    } else {
                        error(U(response: response))
                    }
                }
            case let .failure(moyaError):
                error(U(moyaError: moyaError))
            }
        }
    }
    
    /// Request error handling.
    private func requestErrorHandling<O: BaseMappable>(errorData: U, isRequestingArrayOfObjects: Bool, type: O.Type, nestedAt path: String?, route: T, maxAttempts: Int, success: @escaping (Any) -> Void, error: @escaping (U) -> Void) {
        if maxAttempts >= 0 {
            if let datasource = self.datasource {
                datasource.prerequisiteProcessesForRetryRequest(for: self, previousError: errorData) {
                    self.request(isRequestingArrayOfObjects: isRequestingArrayOfObjects, type: type, nestedAt: path, route: route, maxAttempts: maxAttempts, success: success, error: error)
                }
            } else {
                self.request(isRequestingArrayOfObjects: isRequestingArrayOfObjects, type: type, nestedAt: path, route: route, maxAttempts: maxAttempts, success: success, error: error)
            }
        } else {
            // Out of attemps, do nothing
            print("Out of attemps")
            error(errorData)
        }
    }
}
