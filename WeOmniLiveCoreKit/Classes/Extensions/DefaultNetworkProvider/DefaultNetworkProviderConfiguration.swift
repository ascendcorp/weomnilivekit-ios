//
//  DefaultNetworkProviderConfiguration.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 19/3/21.
//

import Foundation
import Moya

// MARK: - CLASS
public class DefaultNetworkProviderConfiguration {
    
    // MARK: MODEL
    public var baseUrlString: String!
    public var stubBehavior: StubBehavior!
    public var plugins: [PluginType]!
    
    // MARK: INITIALIZATION
    private init() {}
    public convenience init(baseUrlString: String, stubBehavior: StubBehavior = .never, plugins: [PluginType] = [PluginType]()) {
        self.init()
        self.baseUrlString = baseUrlString
        self.stubBehavior = stubBehavior
        self.plugins = plugins
    }
}
