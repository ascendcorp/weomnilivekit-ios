//
//  DefaultNetworkProviderDatasource.swift
//  KyuNetworkExtensions
//
//  Created by Chayanon Ardkham on 6/10/20.
//

import Foundation
import Moya

// MARK: - DATASOURCE
public protocol DefaultNetworkProviderDatasource: class {
    /// User-defined processes before perform any request.
    func prerequisiteProcessesForRequest<T: TargetType, U: DefaultNetworkError>(for provider: DefaultNetworkProvider<T, U>, performRequest: @escaping () -> Void)
    /// User-defined processes before retry any request.
    func prerequisiteProcessesForRetryRequest<T: TargetType, U: DefaultNetworkError>(for provider: DefaultNetworkProvider<T, U>, previousError error: U, performRetryRequest: @escaping () -> Void)
}

// MARK: - DEFAULT DATASOURCE
public extension DefaultNetworkProviderDatasource {
    func prerequisiteProcessesForRequest<T: TargetType, U: DefaultNetworkError>(for provider: DefaultNetworkProvider<T, U>, performRequest: @escaping () -> Void) { performRequest() }
    func prerequisiteProcessesForRetryRequest<T: TargetType, U: DefaultNetworkError>(for provider: DefaultNetworkProvider<T, U>, previousError error: U, performRetryRequest: @escaping () -> Void) { performRetryRequest() }
}
