//
//  DefaultNetworkError.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 23/3/21.
//

import Foundation
import Moya
import ObjectMapper

// MARK: - CLASS
open class DefaultNetworkError: Mappable {
    
    // MARK: MODEL
    public var title: String?
    public var message: String?
    public var response: Response?
    
    // MARK: INITIALIZATION
    public required init(map: Map) {}
    public func mapping(map: Map) {
        title <- map["title"]
        message <- map["message"]
        response <- map["response"]
    }
    
    public required convenience init(title: String?, message: String?) {
        self.init(map: Map(mappingType: .fromJSON, JSON: ["title": title, "message": message]))
    }
    
    public required convenience init(response: Response?) {
        let title = response == nil ? "Unknown Error" : "Error Code \(response!.statusCode)"
        let message = response == nil ? nil : HTTPURLResponse.localizedString(forStatusCode: response!.statusCode)
        self.init(map: Map(mappingType: .fromJSON, JSON: ["title": title, "message": message, "response": response]))
    }
    
    public required convenience init(moyaError: MoyaError) {
        let error = DefaultNetworkError(response: moyaError.response)
        let title = error.title
        let message = error.message
        self.init(map: Map(mappingType: .fromJSON, JSON: ["title": title, "message": message, "response": error.response]))
    }
}
