//
//  +String.swift
//  WeOmniLiveCoreKit
//
//  Created by Sukrit P Mahawongtong on 30/3/2564 BE.
//

import Foundation

extension String {
    public var isValidUrl: Bool {
        guard let url = URL(string: self) else { return false }
        return UIApplication.shared.canOpenURL(url)
    }
}
