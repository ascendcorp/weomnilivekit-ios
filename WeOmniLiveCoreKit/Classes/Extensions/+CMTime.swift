//
//  +CMTime.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 15/3/21.
//

import Foundation
import AVFoundation

extension CMTime {
    public func toInt() -> Int {
        return seconds.isNaN || seconds.isInfinite ? 0 : Int(seconds)
    }
}
