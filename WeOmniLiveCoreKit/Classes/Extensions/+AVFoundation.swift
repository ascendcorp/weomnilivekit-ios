//
//  +AVFoundation.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 15/3/21.
//

import Foundation
import AVFoundation
import UIKit

extension AVPlayer {
    
    public var isPlaying: Bool {
        return timeControlStatus == .playing
    }
    
    public var isLive: Bool {
        return currentItem?.duration == .indefinite
    }
    
    public var currentTimeInSeconds: Int {
        return currentItem?.currentTime().toInt() ?? 0
    }
    
    public var durationInSeconds: Int {
        return currentItem?.duration.toInt() ?? 0
    }
    
    public func seekToLiveIfAble() {
        
        if isLive,
           let currentItem = currentItem,
           let seekableTimeRange = currentItem.seekableTimeRanges.last {
            
            let seekableRange = seekableTimeRange.timeRangeValue
            let seekableStart = CMTimeGetSeconds(seekableRange.start)
            let seekableDuration = CMTimeGetSeconds(seekableRange.duration)
            let livePosition = seekableStart + seekableDuration
            
            let liveTime = CMTime(seconds: livePosition, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            seek(to: liveTime)
            play()
        }
    }
}
