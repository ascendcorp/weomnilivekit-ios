//
//  +Mappable.swift
//  KyuNetworkExtensions
//
//  Created by Chayanon Ardkham on 8/10/20.
//
//  swiftlint:disable syntactic_sugar

import Foundation
import ObjectMapper

extension Mapper {
    
    /// JSONObject from JSONData.
    public func map(jsonData: Data, nestedAt path: String? = nil) -> N? {
        var jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any]
        if let path = path, let serializedJson = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? Dictionary<String, Any> {
            jsonObject = serializedJson.nestedValue(at: path) as? [String: Any]
        }
        return Mapper<N>().map(JSONObject: jsonObject)
    }
    
    /// JSONObjects from JSONData.
    public func mapArray(jsonData: Data, nestedAt path: String? = nil) -> [N]? {
        var jsonObjects = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[String: Any]]
        if let path = path, let serializedJson = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? Dictionary<String, Any> {
            jsonObjects = serializedJson.nestedValue(at: path) as? [[String: Any]]
        }
        return Mapper<N>().mapArray(JSONObject: jsonObjects)
    }
}
