//
//  +UIColor.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 25/3/21.
//

import Foundation
import UIKit

extension UIColor {
    
    /// WLKStreamingRoom's badge status background colors
    public struct BadgeColor {
        public static let live = UIColor(red: 219/255, green: 23/255, blue: 23/255, alpha: 1)
        public static let rerun = UIColor(red: 94/255, green: 145/255, blue: 214/255, alpha: 1)
    }
}
