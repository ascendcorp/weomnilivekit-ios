//
//  +UIViewController.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 15/3/21.
//

import Foundation
import UIKit

extension UIViewController {
    
    private class func initFromNib<T: UIViewController>(_ viewClass: T.Type) -> T {
        return T(nibName: String(describing: viewClass), bundle: Bundle.module)
    }
    
    /// Initialize view by ClassName.
    class func initFromNib() -> Self {
        return initFromNib(self)
    }
}
