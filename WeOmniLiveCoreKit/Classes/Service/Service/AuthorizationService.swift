//
//  AuthorizationService.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 17/3/21.
//

import Foundation
import Moya

public enum AuthorizationAPITarget {
    case getAccessToken
}

// MARK: - MOYA
extension AuthorizationAPITarget: TargetType, AccessTokenAuthorizable {
    public var authorizationType: AuthorizationType? {
        return .basic
    }
    
    public var baseURL: URL {
        return URL(string: "/uaa/oauth")!
    }
    
    public var path: String {
        switch self {
        case .getAccessToken:
            return "/token"
        }
    }
    
    public var headers: [String: String]? {
        return ["Content-Type": "application/x-www-form-urlencoded"]
    }
    
    public var method: Moya.Method {
        switch self {
        case .getAccessToken:
            return .post
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getAccessToken:
            return .requestParameters(parameters: ["grant_type": "client_credentials"], encoding: URLEncoding.default)
        }
    }
}
