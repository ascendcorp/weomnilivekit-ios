//
//  RoomService.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 17/3/21.
//

import Foundation
import Moya

public enum RoomAPITarget {
    case getRoomList(status: [WLKStreamingRoom.Status], size: Int = 20, page: Int = 0, customParameters: [String: Any] = [String: Any]())
    case getRoom(id: String)
}

// MARK: - MOYA
extension RoomAPITarget: TargetType, AccessTokenAuthorizable {
    public var authorizationType: AuthorizationType? {
        return .bearer
    }
    
    public var baseURL: URL {
        return URL(string: "/viewer/rooms")!
    }
    
    public var path: String {
        switch self {
        case .getRoomList:
            return ""
        case .getRoom(let id):
            return "/\(id)"
        }
    }
    
    public var headers: [String: String]? {
        return nil
    }
    
    public var method: Moya.Method {
        switch self {
        case .getRoomList, .getRoom:
            return .get
        }
    }
    
    private var sampleDataFileName: String {
        switch self {
        case .getRoomList:
            return "getRoomList"
        case .getRoom:
            return "getRoom"
        }
    }
    
    public var sampleData: Data {
        do {
            return try String(contentsOfFile: Bundle.module.resourceBundlePath(forResource: sampleDataFileName, ofType: "json") ?? "").data(using: String.Encoding.utf8) ?? Data()
        } catch {
            return Data()
        }
    }
    
    public var task: Task {
        switch self {
        case .getRoomList(let status, let size, let page, let customParameters):
            var parameters = customParameters
            parameters["status.in"] = status.map({ $0.rawValue }).joined(separator: ",")
            parameters["size"] = size
            parameters["page"] = page
            
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
            
        case .getRoom:
            return .requestPlain
        }
    }
}
