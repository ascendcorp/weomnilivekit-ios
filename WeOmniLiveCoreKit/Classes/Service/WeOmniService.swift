//
//  WeOmniService.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 17/3/21.
//

import Foundation
import Moya

// MARK: - CLASS
public class WeOmniService {
    
    // MARK: INSTANCE
    private init() {}
    public convenience init(configuration: WLKConfiguration) {
        self.init()
        self.configuration = configuration
    }
    
    // MARK: VARIABLES
    public var configuration: WLKConfiguration!
    public var accessToken: WLKAccessToken?
    
    public var serverBaseUrlString: String {
        return configuration.environment.serverUrl
    }
    public var moduleBaseUrlString: String {
        return configuration.environment.serverUrl + "/shop/api/projects/" + configuration.projectId
    }
    
    public var basicAccessToken: String {
        return "\(configuration.clientId!):\(configuration.clientSecret!)".data(using: .utf8)!.base64EncodedString()
    }
    public var basicAccessTokenPlugin: AccessTokenPlugin {
        return AccessTokenPlugin { (_) -> String in
            self.basicAccessToken
        }
    }
    
    public var bearerAccessToken: String {
        return accessToken?.accessToken ?? ""
    }
    public var bearerAccessTokenPlugin: AccessTokenPlugin {
        return AccessTokenPlugin { (_) -> String in
            self.bearerAccessToken
        }
    }
    public var webviewBaseUrlString: String {
        return configuration.environment.webviewUrl
    }

    // MARK: PROVIDED SERVICES
    public func authorizationService() -> DefaultNetworkProvider<AuthorizationAPITarget, WLKError> {
        let targetConfiguration = DefaultNetworkProviderConfiguration(baseUrlString: serverBaseUrlString, stubBehavior: configuration.environment.stubBehavior, plugins: [basicAccessTokenPlugin])
        return DefaultNetworkProvider<AuthorizationAPITarget, WLKError>(configuration: targetConfiguration)
    }
    
    public func roomService() -> DefaultNetworkProvider<RoomAPITarget, WLKError> {
        let targetConfiguration = DefaultNetworkProviderConfiguration(baseUrlString: moduleBaseUrlString, stubBehavior: configuration.environment.stubBehavior, plugins: [bearerAccessTokenPlugin])
        return DefaultNetworkProvider<RoomAPITarget, WLKError>(configuration: targetConfiguration, datasource: self)
    }
}

// MARK: - DEFAULTNETWORKPROVIDER CONFIGURATOR
extension WeOmniService: DefaultNetworkProviderDatasource {
    public func prerequisiteProcessesForRequest<T, U>(for provider: DefaultNetworkProvider<T, U>, performRequest: @escaping () -> Void) where T: TargetType, U: DefaultNetworkError {
        if bearerAccessToken.isEmpty {
            authorizationService().requestObject(type: WLKAccessToken.self, route: .getAccessToken) { (accessToken) in
                self.accessToken = accessToken
                performRequest()
            } error: { (_) in
                performRequest()
            }
        } else {
            performRequest()
        }
    }
    
    public func prerequisiteProcessesForRetryRequest<T, U>(for provider: DefaultNetworkProvider<T, U>, previousError error: U, performRetryRequest: @escaping () -> Void) where T: TargetType, U: DefaultNetworkError {
        // Reauthorize and try the request again if needed
        if let response = error.response, response.statusCode == 401 {
            authorizationService().requestObject(type: WLKAccessToken.self, route: .getAccessToken) { (accessToken) in
                self.accessToken = accessToken
                performRetryRequest()
            } error: { (_) in
                performRetryRequest()
            }
        } else {
            performRetryRequest()
        }
    }
}
