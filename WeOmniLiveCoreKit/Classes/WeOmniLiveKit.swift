//
//  WeOmniLiveKit.swift
//  
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import AVFoundation
import UIKit

// MARK: - PROTOCOL
public protocol WeOmniLiveKitProtocol {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
    func applicationDidBecomeActive(_ application: UIApplication)
}

// MARK: - CLASS
open class WeOmniLiveKit: NSObject, WeOmniLiveKitProtocol {
    
    // MARK: INSTANCE
    private override init() {}
    public convenience init(configuration: WLKConfiguration) {
        self.init()
        
        service = WeOmniService(configuration: configuration)
    }
    
    // MARK: VARIABLES
    public var service: WeOmniService!
    
    // MARK: VARIABLES
    public var previousAVAudioSessionCategory: AVAudioSession.Category!
    public var previousAVAudioSessionCategoryOptions: AVAudioSession.CategoryOptions!
    public var previousAVAudioSessionMode: AVAudioSession.Mode!
    
    // MARK: FUNCTION
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        
    }
    
    public func applicationDidBecomeActive(_ application: UIApplication) {
        
        let resumeLiveSelector = NSSelectorFromString("resumeLiveStreamingIfNeeded")
        if responds(to: resumeLiveSelector) {
            perform(resumeLiveSelector)
        }
    }
}
