//
//  WLKError.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 23/3/21.
//

import Foundation
import ObjectMapper
import Moya

public class WLKError: DefaultNetworkError, Error {
    
    // Init
    public override func mapping(map: Map) {
        title <- map["error"]
        message <- map["error_description"]
    }
}
