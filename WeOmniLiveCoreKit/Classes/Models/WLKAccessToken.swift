//
//  WLKAccessToken.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 17/3/21.
//

import Foundation
import ObjectMapper

public class WLKAccessToken: Mappable {
    public var accessToken: String?
    public var tokenType: String?
    public var expiresIn: Int?
    public var scope: String?
    public var iat: Int64?
    public var jti: String?
    
    // Init
    public required init?(map: Map) {}
    public func mapping(map: Map) {
        accessToken <- map["access_token"]
        tokenType <- map["token_type"]
        expiresIn <- map["expires_in"]
        scope <- map["scope"]
        iat <- map["iat"]
        jti <- map["jti"]
    }
}
