//
//  WLKStreamingRoom.swift
//  
//
//  Created by Chayanon Ardkham on 5/3/21.
//

import Foundation
import ObjectMapper

public class WLKStreamingRoom: Mappable {
    
    public var id: String?
    public var user: String?
    public var title: String?
    public var description: String?
    public var numOfView: Int?
    public var status: String?
    public var thumbnailUrl: String?
    public var logoUrl: String?
    public var hlsUrl: String?
    public var products: [WLKProduct]?
    public var promotedProducts: [WLKProduct]?
    public var shoplink: String?
    
    // Init
    public required init?(map: Map) {}
    public func mapping(map: Map) {
        id <- map["id"]
        user <- map["user"]
        title <- map["title"]
        description <- map["description"]
        numOfView <- map["numOfView"]
        status <- map["status"]
        thumbnailUrl <- map["thumbnailUrl"]
        logoUrl <- map["logoUrl"]
        hlsUrl <- map["hlsUrl"]
        products <- map["products"]
        promotedProducts <- map["promotedProducts"]
        shoplink <- map["shoplink"]
    }
}

// MARK: - WLKSTREAMINGROOM.STATUS
extension WLKStreamingRoom {
    public enum Status: String, CaseIterable {
        case unpublished = "UNPUBLISHED"
        case live = "LIVE_NOW"
        case processing = "PROCESSING"
        case rerun = "RERUN"
        case ended = "LIVE_ENDED"
        case banned = "BANNED"
        
        public var title: String {
            switch self {
            case .unpublished:
                return "Unpublished"
            case .live:
                return "Live"
            case .processing:
                return "Processing"
            case .rerun:
                return "Rerun"
            case .ended:
                return "Ended"
            case .banned:
                return "Banned"
            }
        }
        
        public var color: UIColor {
            switch self {
            case .live:
                return UIColor.BadgeColor.live
            case .rerun:
                return UIColor.BadgeColor.rerun
            default:
                return UIColor.lightGray
            }
        }
    }
}
