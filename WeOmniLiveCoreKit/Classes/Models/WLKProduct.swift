//
//  WLKProduct.swift
//  WeOmniLiveCoreKit
//
//  Created by Chayanon Ardkham on 17/3/21.
//

import Foundation
import ObjectMapper

public class WLKProduct: Mappable {
    
    public var id: String?
    
    // Init
    public required init?(map: Map) {}
    public func mapping(map: Map) {
        id <- map["id"]
    }
}
