//
//  WLKConfiguration.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 16/3/21.
//

import Foundation
import Moya

public class WLKConfiguration {
    
    public var clientId: String!
    public var clientSecret: String!
    public var projectId: String!
    public var environment: Environment!
    
    private init() {}
    public convenience init(clientId: String, clientSecret: String, projectId: String, environment: Environment) {
        self.init()
        
        self.clientId = clientId
        self.clientSecret = clientSecret
        self.projectId = projectId
        self.environment = environment
    }
}

// MARK: - WLKCONFIGURATION.ENVIRONMENT
extension WLKConfiguration {
    public enum Environment: String {
        case mock = "Mock"
        case alpha = "Alpha"
        case staging = "Staging"
        case production = "Production"
        
        var serverUrl: String {
            switch self {
            case .mock:
                return ""
            case .alpha:
                return "https://alpha-platform.weomni-test.com"
            case .staging:
                return "https://platform.weomni-test.com"
            case .production:
                return "https://platform.weomni.com"
            }
        }
        
        var webviewUrl: String {
            switch self {
            case .mock:
                return ""
            case .alpha:
                return "https://alpha-live.weomni-test.com"
            case .staging:
                return "https://stg-live.weomni-test.com"
            case .production:
                return "https://live.weomni.com"
            }
        }
        
        var stubBehavior: StubBehavior {
            switch self {
            case .mock:
                return .immediate
            default:
                return .never
            }
        }
    }

}
