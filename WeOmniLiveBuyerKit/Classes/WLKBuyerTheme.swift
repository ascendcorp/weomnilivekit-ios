//
//  Theme.swift
//  WLKBuyerTheme
//
//  Created by Sukrit P Mahawongtong on 29/3/2564 BE.
//

import Foundation

struct WLKBuyerTheme {
    struct Screensizes {
        static let smallestScreenWidth: CGFloat = 375
        static let smallestScreenHeight: CGFloat = UIDevice.current.isNewiPhone() ? 812 : 667
        static let screenWidth = UIScreen.main.bounds.width
        static let screenHeight = UIScreen.main.bounds.height
    }
    static func ratioWidth(value: CGFloat) -> CGFloat {
        return (Screensizes.screenWidth * value) / Screensizes.smallestScreenWidth
    }
    static func ratioHeight(value: CGFloat) -> CGFloat {
        return (Screensizes.screenHeight * value) / Screensizes.smallestScreenHeight
    }
}
