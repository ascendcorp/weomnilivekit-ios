//
//  +UICollectionView.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 15/3/21.
//

import Foundation
import UIKit

extension UICollectionView {
    /// Register reusable cells by ClassName.
    func register<T: UICollectionViewCell>(_ cell: T.Type) {
        register(UINib(nibName: String(describing: cell), bundle: Bundle.module), forCellWithReuseIdentifier: String(describing: cell))
    }
    
    /// Dequeue reusable cells by ClassName.
    func dequeueReusableCell<T: UICollectionViewCell>(_ cell: T.Type, indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withReuseIdentifier: String(describing: cell), for: indexPath) as? T
    }
}
