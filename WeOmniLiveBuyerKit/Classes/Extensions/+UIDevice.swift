//
//  +UIDevice.swift
//  WeOmniLiveBuyerKit
//
//  Created by Sukrit P Mahawongtong on 29/3/2564 BE.
//

import Foundation

public extension UIDevice {
    func isNewiPhone() -> Bool {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return hasTopNotch
        }
        return false
    }
    
    var hasTopNotch: Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.top ?? 0 > 20
        } else {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
    }
}
