// MARK: - Register
extension UIFont {
    static func registerFont(withFilenameString filenameString: String, ofType type: String) {
        class TestClass: NSObject { }
        let bundle = Bundle(for: TestClass.self)
        
        guard let pathForResourceString = bundle.path(forResource: filenameString, ofType: type) else {
            print("UIFont+:  Failed to register font - path for resource not found.")
            return
        }
        
        guard let fontData = NSData(contentsOfFile: pathForResourceString) else {
            print("UIFont+:  Failed to register font - font data could not be loaded.")
            return
        }
        
        guard let dataProvider = CGDataProvider(data: fontData) else {
            print("UIFont+:  Failed to register font - data provider could not be loaded.")
            return
        }
        
        guard let font = CGFont(dataProvider) else {
            print("UIFont+:  Failed to register font - font could not be loaded.")
            return
        }
        
        var errorRef: Unmanaged<CFError>?
        guard CTFontManagerRegisterGraphicsFont(font, &errorRef) else {
            print("UIFont+:  Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
            return
        }
    }
}
