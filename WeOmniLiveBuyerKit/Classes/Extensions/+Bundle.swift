//
//  +Bundle.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 15/3/21.
//

import Foundation

extension Bundle {
    static var module: Bundle {
        return Bundle(for: WLKStreamingRoomListView.self.classForCoder())
    }
    
    var resourceBundle: Bundle? {
        return Bundle(url: resourceURL?.appendingPathComponent("WeOmniLiveBuyerKit.bundle") ?? URL(string: "")!)
    }
    
    func resourceBundlePath(forResource: String?, ofType: String?) -> String? {
        let resourceBundle = self.resourceBundle
        if let path = resourceBundle?.path(forResource: forResource, ofType: ofType) {
            return path
        }
        
        return nil
    }
}
