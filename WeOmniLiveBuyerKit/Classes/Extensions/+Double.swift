//
//  +Double.swift
//  WeOmniLiveBuyerKit
//
//  Created by Chayanon Ardkham on 26/3/21.
//
//  swiftlint:disable identifier_name

import Foundation

extension Double {
    var symbolized: String {
        let K: Double = 1000
        let M: Double = 1000000
        
        switch self {
        case ..<K:
            return String(format: "%.0f", locale: .current, self)
        case K..<M:
            return String(format: "%.1fK", locale: .current, (self / K).rounded(places: 1, rule: .down)).replacingOccurrences(of: ".0", with: "")
        default:
            return String(format: "%.1fM", locale: .current, (self / M).rounded(places: 1, rule: .down)).replacingOccurrences(of: ".0", with: "")
        }
    }
    
    func rounded(places: Int, rule: FloatingPointRoundingRule = .toNearestOrAwayFromZero) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded(rule) / divisor
    }
}
