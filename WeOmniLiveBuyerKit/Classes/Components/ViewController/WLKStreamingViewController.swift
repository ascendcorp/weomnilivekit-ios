//
//  WLKStreamingViewController.swift
//  
//
//  Created by Chayanon Ardkham on 3/3/21.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import WebKit
import WeOmniLiveCoreKit

protocol WLKStreamingDisplayLogic: class {
    func setupMediaPlayer(viewModel: WLKStreaming.MediaPlayer.ViewModel)
    func setupOverlayWebview(viewModel: WLKStreaming.OverlayWebView.ViewModel)
    func displayError()
}

// MARK: - DELEGATION
public protocol WLKStreamingViewControllerDelegate: class {
    func wlkStreamingViewController(_ viewController: WLKStreamingViewController, didSelectUrl url: String)
}

// MARK: - CLASS
public class WLKStreamingViewController: UIViewController, WLKStreamingDisplayLogic {
    
    // MARK: MODEL
    public weak var delegate: WLKStreamingViewControllerDelegate?
    public var pictureInPictureController: AVPictureInPictureController?
            
    private var player: AVPlayer?
    private var playerLayer: AVPlayerLayer?
    private var blurredPlayerLayer: AVPlayerLayer?
    private var playerObserver: NSKeyValueObservation?
        
    private let prefixUrl = "native://"

    // MARK: VIEW
    @IBOutlet private weak var playerContainerView: UIView!
    @IBOutlet private weak var productContainerView: UIView!
    @IBOutlet private weak var blurredPlayerContainerView: UIView!
    @IBOutlet weak var overlayWebView: WKWebView!
    
    @IBOutlet private weak var liveTitleLabel: UILabel!
    @IBOutlet private weak var liveAudienceCountLabel: UILabel!
    
    @IBOutlet private weak var broadcasterProfileImageView: UIImageView!
    @IBOutlet private weak var broadcasterNameLabel: UILabel!
    
    @IBOutlet private weak var playerControlView: WLKAVPlayerControlView!
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
            
    var wlkCustomPopupView: WLKCustomPopupView = WLKCustomPopupView()
    
    var interactor: WLKStreamingBusinessLogic?
    
    // MARK: Setup
    private func setup() {
        let viewController = self
        let interactorStreaming = WLKStreamingInteractor()
        let presenter = WLKStreamingPresenter()
        viewController.interactor = interactorStreaming
        interactorStreaming.presenter = presenter
        presenter.viewController = viewController
    }

    // MARK: VIEW LIFE CYCLE
    public convenience init(wlk: WeOmniLiveKit,
                            streamingRoom: WLKStreamingRoom,
                            overlayWebViewUrlString: String? = nil,
                            isAutoplayEnabled: Bool = true,
                            userId: String,
                            displayName: String) {
        self.init(nibName: String(describing: Self.self), bundle: Bundle.module)
        setup()
        initWLKStreamingViewController(wlk: wlk,
                                       streamingRoom: streamingRoom,
                                       overlayWebViewUrlString: overlayWebViewUrlString,
                                       isAutoplayEnabled: isAutoplayEnabled,
                                       userId: userId,
                                       displayName: displayName)
    }
    
    private func webviewConfiguration() {
        overlayWebView.navigationDelegate = self
        overlayWebView.isOpaque = false
        overlayWebView.backgroundColor = .clear
        overlayWebView.scrollView.backgroundColor = .clear
        overlayWebView.scrollView.bounces = false
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        wlkCustomPopupView.delegate = self
        
        broadcasterProfileImageView.layer.cornerRadius = broadcasterProfileImageView.frame.height / 2
        
        productContainerView.layer.cornerRadius = 8
        productContainerView.layer.borderWidth = 1
        productContainerView.layer.borderColor = UIColor.white.cgColor
                
        liveTitleLabel.text = interactor?.streamingRoom.title
        
        webviewConfiguration()
        
        if let url = interactor?.overlayWebViewUrlString {
            setWebViewContentUrl(url: url)
        } else {
            fetchRoomDetail()
        }
    }
        
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        navigationController?.tabBarController?.tabBar.isHidden = true
        pictureInPictureController?.stopPictureInPicture()
        interactor?.wlk?.configureAVAudioSessionForLiveStreaming()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        resumeLiveStreamingIfNeeded()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.tabBarController?.tabBar.isHidden = false
        pictureInPictureController?.startPictureInPicture()
    }
    
    // MARK: INITIALIZATION
    public func initWLKStreamingViewController(wlk: WeOmniLiveKit,
                                               streamingRoom: WLKStreamingRoom,
                                               overlayWebViewUrlString: String? = nil,
                                               isAutoplayEnabled: Bool = true,
                                               userId: String,
                                               displayName: String) {
        interactor?.wlk = wlk
        interactor?.streamingRoom = streamingRoom
        interactor?.overlayWebViewUrlString = overlayWebViewUrlString
        interactor?.userId = userId
        interactor?.displayName = displayName
        interactor?.isAutoplayEnabled = isAutoplayEnabled
    }
    
    public func autoplayLiveStreamingIfAvailable() {
        guard let isAutoplayEnabled = interactor?.isAutoplayEnabled,
              let inAutoPlayed = interactor?.isAutoPlayed else { return }
        if isAutoplayEnabled && !inAutoPlayed,
        let urlString = interactor?.streamingRoom.hlsUrl,
           let mediaUrl = URL(string: urlString) {
            startLiveStreaming(with: mediaUrl)
            interactor?.isAutoPlayed = true
        }
    }
    
    public func startLiveStreaming(with mediaUrl: URL) {
        
        stopLiveStreaming()
        
        player = AVPlayer(url: mediaUrl)
        
        if let player = player {
            
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = playerContainerView.bounds
            
            if let playerLayer = playerLayer {
                playerContainerView.layer.addSublayer(playerLayer)
                
                pictureInPictureController = AVPictureInPictureController(playerLayer: playerLayer)
                pictureInPictureController?.delegate = self
            }
            
            player.play()
            
            playerControlView.initWLKAVPlayerControlView(player: player)
            
            // Observe when player starts playing
            playerObserver = player.observe(\.timeControlStatus) { [self] (_, _) in
                setIfPictureInPictureControllerRequiresLinearPlayback()
                addBlurredPlayerLayerInBackground()
            }
        }
    }
    
    public func stopLiveStreaming() {
        
        if let player = player,
           let playerLayer = playerLayer {
            
            playerLayer.removeFromSuperlayer()
            self.playerLayer = nil
            
            blurredPlayerLayer?.removeFromSuperlayer()
            self.blurredPlayerLayer = nil
            
            player.pause()
            self.player = nil
            
            pictureInPictureController?.stopPictureInPicture()
            pictureInPictureController = nil
        }
    }
    
    public func resumeLiveStreamingIfNeeded() {
        if let player = player, player.timeControlStatus == .paused && player.isLive {
            player.seekToLiveIfAble()
        }
    }
    
    private func addBlurredPlayerLayerInBackground() {
        
        blurredPlayerLayer = AVPlayerLayer(player: player)
        if let blurredPlayerLayer = blurredPlayerLayer {
            
            for subview in blurredPlayerContainerView.subviews {
                subview.removeFromSuperview()
            }
            
            blurredPlayerLayer.videoGravity = .resizeAspectFill
            blurredPlayerLayer.frame = blurredPlayerContainerView.bounds
            blurredPlayerContainerView.layer.addSublayer(blurredPlayerLayer)
            
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = blurredPlayerContainerView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurredPlayerContainerView.addSubview(blurEffectView)
        }
    }
    
    @IBAction private func didTapOnBackButton(_ sender: Any) {
        stopLiveStreaming()
        interactor?.wlk?.restoreToPreviousAVAudioSessionSettings()
        if let navigationController = navigationController {
            navigationController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - AVPICTUREINPICTURE CONTROLLER DELEGATE
extension WLKStreamingViewController: AVPictureInPictureControllerDelegate {
    private func setIfPictureInPictureControllerRequiresLinearPlayback() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            if #available(iOS 14.0, *) {
                pictureInPictureController?.requiresLinearPlayback = player?.isLive ?? false
            } else {
                pictureInPictureController?.setValue(player?.isLive ?? false, forKey: "requiresLinearPlayback")
            }
            
            activityIndicatorView.isHidden = true
        }
    }
    
    public func pictureInPictureController(_ pictureInPictureController: AVPictureInPictureController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        
        navigationController?.popToViewController(self, animated: true)
        
        DispatchQueue.main.async {
            completionHandler(true)
        }
    }
}

extension WLKStreamingViewController {
    func addOverlayBackground() {
        self.view.mask = UIView(frame: self.view.frame)
        self.view.mask?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func removeOverlayBackground() {
        self.view.mask = nil
    }

    func displayError() {
        hideActivityIndicator()
        addOverlayBackground()
        let expectedXPos = WLKBuyerTheme.ratioWidth(value: 16)
        let popupHeight = WLKBuyerTheme.ratioHeight(value: 369)
        let popupWidth = WLKBuyerTheme.Screensizes.screenWidth - (expectedXPos * 2)
        let expectedYPos = (WLKBuyerTheme.Screensizes.screenHeight / 2) - (popupHeight / 2)
        wlkCustomPopupView.frame = CGRect(x: expectedXPos,
                                          y: expectedYPos,
                                          width: popupWidth,
                                          height: popupHeight)
        UIApplication.shared.addSubViewToWindow(view: wlkCustomPopupView)
    }
    
    func setupMediaPlayer(viewModel: WLKStreaming.MediaPlayer.ViewModel) {
        hideActivityIndicator()
    }
    
    func setupOverlayWebview(viewModel: WLKStreaming.OverlayWebView.ViewModel) {
        guard let baseUrl = interactor?.wlk?.service.webviewBaseUrlString,
              let projectId = interactor?.wlk?.service.configuration.projectId,
              let userId = interactor?.userId,
              let displayName = interactor?.displayName,
              let encodedDisplayName = displayName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        let roomId = viewModel.roomId
        let url = "\(baseUrl)/rooms/\(projectId)-\(roomId)?userId=\(userId)&displayName=\(encodedDisplayName)&tp=false&mode=native"
        setWebViewContentUrl(url: url)
    }
}

extension WLKStreamingViewController {
    private func fetchRoomDetail() {
        activityIndicatorView.isHidden = false
        interactor?.fetchRoomDetail()
    }
}

extension WLKStreamingViewController {
    private func setWebViewContentUrl(url: String) {
        if url.isValidUrl {
            loadWebView(with: url)
        } else {
            displayError()
        }
    }
    
    private func dismissCustomPopupView() {
        removeOverlayBackground()
        wlkCustomPopupView.removeFromSuperview()
    }
    
    private func showActivityIndicator() {
        activityIndicatorView.isHidden = false
    }
    
    private func hideActivityIndicator() {
        activityIndicatorView.isHidden = true
    }
}

extension WLKStreamingViewController: WLKCustomPopupViewDelegate {
    public func wlkCustomPopupViewDidSelectCancel(_ wlkCustomPopupView: WLKCustomPopupView) {
        dismissCustomPopupView()
        goBack()
    }
    
    public func wlkCustomPopupViewDidSelectRetry(_ wlkCustomPopupView: WLKCustomPopupView) {
        dismissCustomPopupView()
        fetchRoomDetail()
    }
}

extension WLKStreamingViewController: WKUIDelegate, WKNavigationDelegate {
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideActivityIndicator()
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        hideActivityIndicator()
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            decisionHandler(.cancel)
            return
        }
        if url.absoluteString.contains(prefixUrl+"close") {
            goBack()
            decisionHandler(.cancel)
            return
        } else if url.absoluteString.contains(prefixUrl) {
            let replacedUrl = url.absoluteString.replacingOccurrences(of: prefixUrl+"redirect?link=",
                                                       with: "",
                                                       options: .caseInsensitive)
            delegate?.wlkStreamingViewController(self, didSelectUrl: replacedUrl)
            decisionHandler(.cancel)
            return
        }

        decisionHandler(.allow)
    }
}

extension WLKStreamingViewController {
    private func goBack() {
        navigationController?.popViewController(animated: true)
    }
}
