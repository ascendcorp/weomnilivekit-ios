//
//  WLKStreamingInteractor.swift
//  Pods
//
//  Created by Sukrit Mahawongthong on 24/3/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.

import UIKit
import WeOmniLiveCoreKit

protocol WLKStreamingBusinessLogic {
    var streamingRoom: WLKStreamingRoom! { get set }
    var wlk: WeOmniLiveKit? { get set }
    var userId: String? { get set }
    var displayName: String? { get set }
    var overlayWebViewUrlString: String? { get set }
    var isAutoPlayed: Bool? { get set }
    var isAutoplayEnabled: Bool? { get set }
    func fetchRoomDetail()
}

class WLKStreamingInteractor: WLKStreamingBusinessLogic {
    var streamingRoom: WLKStreamingRoom!
    var wlk: WeOmniLiveKit?
    var isAutoPlayed: Bool? = false
    var isAutoplayEnabled: Bool? = true
    var userId: String?
    var displayName: String?
    var overlayWebViewUrlString: String?
    
    // MARK: - Properties
    var presenter: WLKStreamingPresentationLogic?
    var worker: WLKStreamingWorker?

    // MARK: - Initializer
    init(worker: WLKStreamingWorker = WLKStreamingWorker()) {
        self.worker = worker
    }
}

extension WLKStreamingInteractor {
    func fetchRoomDetail() {
        if let roomId = streamingRoom.id,
           let wlk = wlk {
            worker?.getRoomDetail(wlk: wlk, id: roomId, success: { [weak self] result in
                if let strongSelf = self {
                    let response = WLKStreaming.FetchRoomDetail.Response(result: .success(result))
                    strongSelf.presenter?.presentRoomDetailResult(response: response)
                }
            }, failure: { [weak self ] error in
                if let strongSelf = self {
                    let response = WLKStreaming.FetchRoomDetail.Response(result: .failure(error))
                    strongSelf.presenter?.presentRoomDetailResult(response: response)
                }
            })
        } else {
            let error = WLKError.init(title: "error", message: "Invalid data")
            let response = WLKStreaming.FetchRoomDetail.Response(result: .failure(error))
            presenter?.presentRoomDetailResult(response: response)
        }
    }
}
