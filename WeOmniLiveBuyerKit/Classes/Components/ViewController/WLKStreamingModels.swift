//
//  WLKStreamingModels.swift
//  Pods
//
//  Created by Sukrit Mahawongthong on 24/3/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import WeOmniLiveCoreKit

enum WLKStreaming {
    // MARK: - Models
    enum MediaPlayer {
        struct ViewModel {
            let mediaUrl: String
        }
    }
    enum OverlayWebView {
        struct ViewModel {
            let roomId: String
        }
    }
    enum ErrorHandling {
        struct ViewModel {
            let errorMessage: String
        }
    }
    enum FetchRoomDetail {
        struct Request {
            let id: String
        }

        struct Response {
            let result: Result<WLKStreamingRoom, WLKError>
        }
    }
}
