//
//  WLKStreamingPresenter.swift
//  Pods
//
//  Created by Sukrit Mahawongthong on 24/3/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import WeOmniLiveCoreKit

protocol WLKStreamingPresentationLogic {
    func presentRoomDetailResult(response: WLKStreaming.FetchRoomDetail.Response)
}

class WLKStreamingPresenter: WLKStreamingPresentationLogic {
    // MARK: - Properties

    weak var viewController: WLKStreamingDisplayLogic?

}

// MARK: - WLKStreamingPresentationLogic
extension WLKStreamingPresenter {
    func presentRoomDetailResult(response: WLKStreaming.FetchRoomDetail.Response) {
        switch response.result {
        case .success(let data):
            if let status = data.status, let roomId = data.id {
                switch status {
                case WLKStreamingRoom.Status.live.rawValue,
                     WLKStreamingRoom.Status.rerun.rawValue:
                    let webViewViewModel = WLKStreaming.OverlayWebView.ViewModel(roomId: roomId)
                    viewController?.setupOverlayWebview(viewModel: webViewViewModel)
                default:
                    viewController?.displayError()
                }
            } else {
                viewController?.displayError()
            }
        case .failure:
            viewController?.displayError()
        }
    }
}
