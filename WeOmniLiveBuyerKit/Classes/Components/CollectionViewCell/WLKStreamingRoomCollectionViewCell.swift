//
//  WLKStreamingRoomCollectionViewCell.swift
//  
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import UIKit
import WeOmniLiveCoreKit
import SDWebImage

// MARK: - CLASS
public class WLKStreamingRoomCollectionViewCell: UICollectionViewCell {
    
    // MARK: MODEL
    private var streamingRoom: WLKStreamingRoom!
    
    // MARK: VIEW
    @IBOutlet private weak var liveImageView: UIImageView!
    @IBOutlet private weak var liveBadgeLabel: UILabel!
    @IBOutlet private weak var liveBadgeContainerView: UIView!
    @IBOutlet private weak var liveAudienceCountLabel: UILabel!
    @IBOutlet private weak var liveAudienceContainerView: UIView!
    @IBOutlet private weak var liveTitleLabel: UILabel!
    @IBOutlet private weak var gradientView: UIView!
    
    // MARK: VIEW LIFE CYCLE
    public override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        // Set status font
        let font = liveBadgeLabel.font.withSize(liveBadgeContainerView.frame.height * 10 / 16)
        liveBadgeLabel.font = font
        liveAudienceCountLabel.font = font
        
        liveTitleLabel.font = liveTitleLabel.font.withSize(frame.height * 16 / 280)
        
        // Set status corners
        let cornerRadius = liveAudienceContainerView.frame.height / 8
        liveBadgeContainerView.layer.cornerRadius = cornerRadius
        liveAudienceContainerView.layer.cornerRadius = cornerRadius
        
        liveBadgeContainerView.clipsToBounds = true
        liveAudienceContainerView.clipsToBounds = true
        
        setGradient()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    // MARK: INITIALIZATION
    public func initWLKStreamingRoomCollectionViewCell(streamingRoom: WLKStreamingRoom) {
        self.streamingRoom = streamingRoom
        
        liveImageView.sd_setImage(with: URL(string: streamingRoom.thumbnailUrl ?? ""), completed: nil)
        liveTitleLabel.text = streamingRoom.title
        
        if let status = WLKStreamingRoom.Status(rawValue: streamingRoom.status ?? "LIVE_NOW") {
            liveBadgeLabel.text = status.title
            liveBadgeContainerView.backgroundColor = status.color
        }
        
        liveAudienceCountLabel.text = "\(Double(streamingRoom.numOfView ?? 0).symbolized)"
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}

// MARK: - PRIVATE MACHANISM
extension WLKStreamingRoomCollectionViewCell {
    func setGradient() {
        // Remove all existed sublayers
        for layer in gradientView.layer.sublayers ?? [CALayer]() {
            layer.removeFromSuperlayer()
        }
        
        // Add gradient sublayer
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = gradientView.bounds
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
