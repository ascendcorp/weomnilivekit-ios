//
//  WLKStreamingRoomListView.swift
//  
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import UIKit
import WeOmniLiveCoreKit

// MARK: - DELEGATION
public protocol WLKStreamingRoomListViewDelegate: class {
    func wlkStreamingRoomListView(_ wlkStreamingRoomListView: WLKStreamingRoomListView, didSelectStreamingRoom streamingRoom: WLKStreamingRoom)
    func wlkStreamingRoomListViewDidPullToRefreshTriggered(_ wlkStreamingRoomListView: WLKStreamingRoomListView)
    func wlkStreamingRoomListViewDidLoadMoreTriggered(_ wlkStreamingRoomListView: WLKStreamingRoomListView)
}

public extension WLKStreamingRoomListViewDelegate {
    func wlkStreamingRoomListView(_ wlkStreamingRoomListView: WLKStreamingRoomListView, didSelectStreamingRoom streamingRoom: WLKStreamingRoom) {}
    func wlkStreamingRoomListViewDidPullToRefreshTriggered(_ wlkStreamingRoomListView: WLKStreamingRoomListView) {}
    func wlkStreamingRoomListViewDidLoadMoreTriggered(_ wlkStreamingRoomListView: WLKStreamingRoomListView) {}
}

// MARK: CLASS
public class WLKStreamingRoomListView: UIView {
    
    private let cardWidthReference: CGFloat = 180
    private let cardheightReference: CGFloat = 280
    
    // MARK: MODEL
    public weak var delegate: WLKStreamingRoomListViewDelegate?
    public var isPullToRefreshEnabled = true
    public var isLoadMoreEnabled = true
    
    private var status: [WLKStreamingRoom.Status] = []
    private var numberOfItemsInPage: Int = 20
    private var customParameters: [String: Any] = [String: Any]()
    
    private var wlk: WeOmniLiveKit?
    private var scrollDirection: UICollectionView.ScrollDirection = .vertical
    
    private var streamingRooms = [WLKStreamingRoom]()
    private var refreshControl: UIRefreshControl!
    private var isUsingAutomaticRenderedMode = true
    
    private var numberOfDisplayingPage: Int {
        return Int(floor(Double(streamingRooms.count / numberOfItemsInPage)))
    }
    
    // MARK: VIEW
    @IBOutlet public weak var collectionView: UICollectionView!
    
    @IBOutlet private weak var horizontalErrorView: UIView!
    @IBOutlet private weak var horizontalErrorTitleLabel: UILabel!
    @IBOutlet private weak var horizontalErrorMessageLabel: UILabel!
    @IBOutlet private weak var horizontalRetryButton: UIButton!
    
    @IBOutlet private weak var verticalErrorView: UIView!
    @IBOutlet private weak var verticalErrorImageView: UIImageView!
    @IBOutlet private weak var verticalErrorTitleLabel: UILabel!
    @IBOutlet private weak var verticalErrorMessageLabel: UILabel!
    @IBOutlet private weak var verticalRetryButton: UIButton!
    
    // MARK: VIEW LIFE CYCLE
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        guard let view = initFromNib() else { return }
        view.frame = self.bounds
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        horizontalRetryButton.layer.cornerRadius = 8
        verticalRetryButton.layer.cornerRadius = 8
        
        horizontalErrorView.isHidden = true
        verticalErrorView.isHidden = true
        
        verticalErrorImageView.image = UIImage(named: "electric-tower", in: Bundle.module.resourceBundle, compatibleWith: nil)
        
        self.addSubview(view)
    }
    
    public convenience init(wlk: WeOmniLiveKit, scrollDirection: UICollectionView.ScrollDirection, streamingRooms: [WLKStreamingRoom]) {
        self.init(wlk: wlk, scrollDirection: scrollDirection, status: [])
        self.wlk = wlk
        self.streamingRooms = streamingRooms
        
        collectionView.refreshControl = nil
        
        refreshControl.beginRefreshing()
        insertStreamingRoomListWithAnimation(streamingRooms: streamingRooms, isRefreshing: true)
    }
    
    public convenience init(wlk: WeOmniLiveKit, scrollDirection: UICollectionView.ScrollDirection, status: [WLKStreamingRoom.Status], numberOfItemsInPage: Int = 20, customParameters: [String: Any] = [String: Any]()) {
        self.init()
        self.wlk = wlk
        self.scrollDirection = scrollDirection
        self.status = status
        self.numberOfItemsInPage = numberOfItemsInPage
        self.customParameters = customParameters
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = scrollDirection
        }
        
        // Pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        collectionView.refreshControl = (scrollDirection == .vertical && isUsingAutomaticRenderedMode && isPullToRefreshEnabled) ? refreshControl : nil
        collectionView.alwaysBounceVertical = scrollDirection == .vertical
        collectionView.alwaysBounceHorizontal = scrollDirection == .horizontal
        
        collectionView.register(WLKStreamingRoomCollectionViewCell.self)
        
        refreshControl.beginRefreshing()
        loadMoreRoomList(isRefreshing: false)
    }
    
    // MARK: ACTIONS
    @IBAction private func didTapOnHorizontalRetryButton(_ sender: Any) {
        refreshStreamingRoomList()
    }
    
    @IBAction private func didTapOnVerticalRetryButton(_ sender: Any) {
        refreshStreamingRoomList()
        refreshControl.beginRefreshing()
    }
    
    @objc private func refresh(_ sender: AnyObject) {
        refreshStreamingRoomList()
    }
    
    private func displayAlert(title: String) {
        if let viewController = UIApplication.topViewController() {
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            viewController.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: - UICOLLECTIONVIEW DELEGATE, UICOLLECTIONVIEW DATASOURCE, UICOLLECTIONVIEW DELEGATE FLOW LAYOUT
extension WLKStreamingRoomListView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return streamingRooms.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(WLKStreamingRoomCollectionViewCell.self, indexPath: indexPath) {
            let streamingRoom = streamingRooms[indexPath.row]
            cell.initWLKStreamingRoomCollectionViewCell(streamingRoom: streamingRoom)
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let streamingRoom = streamingRooms[indexPath.row]
        
        delegate?.wlkStreamingRoomListView(self, didSelectStreamingRoom: streamingRoom)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            switch layout.scrollDirection {
            case .horizontal:
                return CGSize(width: frame.height / cardheightReference * cardWidthReference, height: frame.height)
            case .vertical:
                let maximumWidthForAllColumns = floor(frame.width - layout.sectionInset.left - layout.sectionInset.right + layout.minimumInteritemSpacing)
                let maximumNumberOfColumns = max(floor(maximumWidthForAllColumns / cardWidthReference), 2)
                let maximumWidthForColumn = maximumWidthForAllColumns / maximumNumberOfColumns - layout.minimumInteritemSpacing
                return CGSize(width: maximumWidthForColumn, height: maximumWidthForColumn / cardWidthReference * cardheightReference)
            @unknown default:
                break
            }
        }
        
        return .zero
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == streamingRooms.count - 1 && streamingRooms.count % numberOfItemsInPage == 0 && isLoadMoreEnabled {
            loadMoreRoomList(isRefreshing: false)
        }
    }
}

// MARK: - API CONNECTION
extension WLKStreamingRoomListView {
    private func loadMoreRoomList(isRefreshing: Bool) {
        if isUsingAutomaticRenderedMode {
            wlk?.getStreamingRoomList(status: status, size: numberOfItemsInPage, page: isRefreshing ? 0 : numberOfDisplayingPage, customParameters: customParameters, success: { (streamingRooms) in
                self.displayErrorViewIfNeeded(isNeeded: false)
                self.insertStreamingRoomListWithAnimation(streamingRooms: streamingRooms, isRefreshing: isRefreshing)
            }, failure: { (_) in
                self.displayErrorViewIfNeeded(isNeeded: true)
                self.refreshControl.endRefreshing()
            })
        }
        
        delegate?.wlkStreamingRoomListViewDidLoadMoreTriggered(self)
    }
    
    private func refreshStreamingRoomList() {
        displayErrorViewIfNeeded(isNeeded: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { [self] in
            
            if isUsingAutomaticRenderedMode {
                loadMoreRoomList(isRefreshing: true)
            }
            
            delegate?.wlkStreamingRoomListViewDidPullToRefreshTriggered(self)
        }
    }
    
    public func insertStreamingRoomListWithAnimation(streamingRooms: [WLKStreamingRoom], isRefreshing: Bool) {
        refreshControl.endRefreshing()
        
        if isRefreshing {
            self.streamingRooms.removeAll()
            collectionView.reloadData()
        }
        
        var insertIndexPaths = [IndexPath]()
        for index in streamingRooms.indices {
            insertIndexPaths.append(IndexPath(row: self.streamingRooms.count + index, section: 0))
        }
        
        DispatchQueue.main.async {
            self.streamingRooms.append(contentsOf: streamingRooms)
            self.collectionView.insertItems(at: insertIndexPaths)
        }
    }
    
    private func displayErrorViewIfNeeded(isNeeded: Bool) {
        horizontalErrorView.isHidden = scrollDirection != .horizontal || !isNeeded
        verticalErrorView.isHidden = scrollDirection != .vertical || !isNeeded
    }
}
