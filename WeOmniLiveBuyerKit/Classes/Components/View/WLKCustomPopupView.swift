//
//  WLKCustomPopupView.swift
//  Pods
//
//  Created by Sukrit P Mahawongtong on 25/3/2564 BE.
//

import Foundation
import UIKit

// MARK: - DELEGATION
public protocol WLKCustomPopupViewDelegate: class {
    func wlkCustomPopupViewDidSelectCancel(_ wlkCustomPopupView: WLKCustomPopupView)
    func wlkCustomPopupViewDidSelectRetry(_ wlkCustomPopupView: WLKCustomPopupView)
}

// MARK: - CLASS
public class WLKCustomPopupView: UIView {
    
    // MARK: MODEL
    public weak var delegate: WLKCustomPopupViewDelegate?
        
    // MARK: VIEW

    @IBOutlet private var viewContainer: UIView!
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var retryButton: UIButton!
    @IBOutlet private weak var headerImageView: UIImageView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    
    // MARK: VIEW LIFE CYCLE
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
        
    private func commonInit() {
        guard let view = initFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        viewContainer.layer.cornerRadius = WLKBuyerTheme.ratioHeight(value: 13)
        headerImageView.image = UIImage(named: "gradient-warning", in: Bundle.module.resourceBundle, compatibleWith: nil)
        
        cancelButton.setBackgroundImage(UIImage(named: "grey-bg-button", in: Bundle.module.resourceBundle, compatibleWith: nil), for: .normal)
        retryButton.setBackgroundImage(UIImage(named: "gradient-bg-button", in: Bundle.module.resourceBundle, compatibleWith: nil), for: .normal)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        isHidden = true
    }
        
    // MARK: ACTIONS
    @IBAction func didTabOnCancel(_ sender: Any) {
        delegate?.wlkCustomPopupViewDidSelectCancel(self)
    }
    
    @IBAction func didTapOnRetry(_ sender: Any) {
        delegate?.wlkCustomPopupViewDidSelectRetry(self)
    }
}
