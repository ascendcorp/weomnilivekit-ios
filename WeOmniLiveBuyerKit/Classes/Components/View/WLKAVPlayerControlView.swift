//
//  WLKAVPlayerControlView.swift
//
//
//  Created by Chayanon Ardkham on 8/3/21.
//

import Foundation
import AVFoundation
import UIKit
import WeOmniLiveCoreKit

// MARK: - DELEGATION
public protocol WLKAVPlayerControlViewDelegate: class {
    
}

// MARK: - CLASS
public class WLKAVPlayerControlView: UIView {
    
    // MARK: MODEL
    public weak var delegate: WLKAVPlayerControlViewDelegate?
    
    private var player: AVPlayer?
    private var playerTimeObserver: Any?
    private var playerEndTimeObserver: Any?
    private var playerTimeControlStatusObserver: NSKeyValueObservation?
    private var playerCurrentTimeObserver: NSKeyValueObservation?
    
    private var currentPlayerTimeInSecond: Int {
        return Int(seekSlider.value)
    }
    
    // MARK: VIEW
    @IBOutlet private weak var playAndPauseButton: UIButton!
    @IBOutlet private weak var currentTimeLabel: UILabel!
    @IBOutlet private weak var remainingTimeLabel: UILabel!
    @IBOutlet private weak var seekSlider: UISlider!
    
    // MARK: VIEW LIFE CYCLE
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func commonInit() {
        guard let view = initFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        var thumbImage: UIImage?
        if #available(iOS 13.0, *) {
            thumbImage = UIImage(systemName: "circle.fill")
        }
        
        seekSlider.setThumbImage(thumbImage, for: .normal)
        seekSlider.setThumbImage(thumbImage, for: .highlighted)
        
        isHidden = true
    }
    
    // MARK: INITIALIZATION
    public func initWLKAVPlayerControlView(player: AVPlayer) {
        self.player = player
        
        seekSlider.addTarget(self, action: #selector(seekSliderDidBeginEditing), for: [.touchDown])
        seekSlider.addTarget(self, action: #selector(seekSliderDidEndEditing), for: [.touchUpInside, .touchUpOutside])
        
        playerTimeControlStatusObserver = player.observe(\.timeControlStatus) { [self] (player, _) in
            seekSlider.maximumValue = Float(player.durationInSeconds)
            setTimeObserver()
            updatePlayAndPauseButtonImage()
        }
        
        // Loop the player if it's ended
        playerEndTimeObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { _ in
            if let player = self.player {
                player.seek(to: CMTime.zero)
                self.play()
            }
        }
    }
    
    // MARK: ACTIONS
    @IBAction private func didTapOnPlayAndPauseButton(_ sender: UIButton) {
        if player?.isPlaying ?? false {
            pause()
        } else {
            play()
        }
    }
}

// MARK: - SEEKSLIDER HANDLERS
extension WLKAVPlayerControlView {
    
    @IBAction private func seekSliderValueChanged(sender: UISlider) {
        updateTimeLabels()
    }
    
    @objc private func seekSliderDidBeginEditing(sender: UISlider) {
        pause()
    }
    
    @objc private func seekSliderDidEndEditing(sender: UISlider) {
        if let player = player {
            let time = CMTime(seconds: Double(currentPlayerTimeInSecond), preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            player.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero)
            play()
        }
    }
}

// MARK: - FUNCTIONS
extension WLKAVPlayerControlView {
    
    private func updatePlayAndPauseButtonImage() {
        var image: UIImage?
        if #available(iOS 13.0, *) {
            image = UIImage(systemName: player?.isPlaying ?? false ? "pause.fill" : "play.fill")
        }
        playAndPauseButton.setImage(image, for: .normal)
    }
    
    private func pause() {
        removeTimeObserver()
        player?.pause()
    }
    
    private func play() {
        setTimeObserver()
        player?.play()
    }
    
    private func setTimeObserver() {
        let interval = CMTime(seconds: 0.5, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        playerTimeObserver = player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { [self] time in
            if player?.isPlaying ?? false {
                seekSlider.value = Float(time.toInt())
                seekSliderValueChanged(sender: seekSlider)
            }
        }
    }
    
    private func removeTimeObserver() {
        // Remove existed time observer
        if playerTimeObserver != nil {
            player?.removeTimeObserver(playerTimeObserver!)
            playerTimeObserver = nil
        }
    }
    
    private func updateTimeLabels() {
        if let player = player {
            currentTimeLabel.text = toTimeString(seconds: currentPlayerTimeInSecond)
            remainingTimeLabel.text = toTimeString(seconds: player.durationInSeconds - currentPlayerTimeInSecond)
            
            currentTimeLabel.isHidden = player.isLive
            remainingTimeLabel.isHidden = player.isLive
            seekSlider.isHidden = player.isLive
            
            isHidden = false
        }
    }
    
    private func toTimeString(seconds: Int) -> String {
        let hours = seconds / 3600
        let minutes = (seconds % 3600) / 60
        let seconds = (seconds % 3600) % 60
        return String(format: "%1d:%02d:%02d", hours, minutes, seconds)
    }
}
