//
//  WLKOverlayBackgroundView.swift
//  Pods
//
//  Created by Sukrit P Mahawongtong on 26/3/2564 BE.
//

import Foundation
import UIKit

class WLKOverlayBackgroundView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeUI()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func initializeUI() {
        backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        addSubview(UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.dark)))
    }
}
