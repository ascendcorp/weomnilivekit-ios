//
//  WLKBuyerKit.swift
//  
//
//  Created by Chayanon Ardkham on 1/3/21.
//

import Foundation
import UIKit
import WeOmniLiveCoreKit

// MARK: - MISCELLANEOUS
extension WeOmniLiveKit {
    @objc public func resumeLiveStreamingIfNeeded() {
        if let viewController = UIApplication.topViewController() as? WLKStreamingViewController {
            viewController.pictureInPictureController?.stopPictureInPicture()
            viewController.resumeLiveStreamingIfNeeded()
        }
    }
}
