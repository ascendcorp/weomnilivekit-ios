[![CI Status](https://img.shields.io/travis/Chayanon Ardkham/WeOmniLiveKit.svg?style=flat)](https://travis-ci.org/Chayanon Ardkham/WeOmniLiveKit)
[![Version](https://img.shields.io/cocoapods/v/WeOmniLiveKit.svg?style=flat)](https://cocoapods.org/pods/WeOmniLiveKit)
[![License](https://img.shields.io/cocoapods/l/WeOmniLiveKit.svg?style=flat)](https://cocoapods.org/pods/WeOmniLiveKit)
[![Platform](https://img.shields.io/cocoapods/p/WeOmniLiveKit.svg?style=flat)](https://cocoapods.org/pods/WeOmniLiveKit)

# WeOmniLiveKit
* [Integration](#markdown-header-integration)
    * [CocoaPods](#markdown-header-cocoapods)
* [WeOmniLiveKit for Buyer](#markdown-header-buyer)
    * [View Components](#markdown-header-view-components)
        * [WLKStreamingRoomListView](#markdown-header-wlkstreamingroomlistview)
        * [WLKStreamingViewController](#markdown-header-wlkstreamingviewcontroller)
    * [Available API Services](#markdown-header-available-api-services)
        * [WLKStreamingRoom list from API](#markdown-header-wlkstreamingroom-list-from-api)
        * [WLKStreamingRoom detail from API](#buyer-availableapiservices-wlkstreamingroom-detail-from-api)
* [WeOmniLiveKit Example Project](#markdown-header-weomnilivekit-example-project)
    * [Installation](#markdown-header-installation)

WeOmmiLiveKit's full documentation can be found [here](https://coda.io/d/WeMall-Live-commerce_dg1ibxjKWdr).

# Integration

## CocoaPods
### Step 1: Install CocoaPods
```bash
$ sudo gem install cocoapods
```

### Step 2: Podfile Configuration
```ruby
target 'YourAppTarget' do
    pod 'WeOmniLiveKit'
end
```
*WeOmniLiveKit* has subspec to supports module selection.

|  Subspec | Details |
|  ---- | ---- |
|  `pod 'WeOmniLiveKit/Core'` | WeOmniLiveKit’s core functions. |
|  `pod 'WeOmniLiveKit/Buyer'` | WeOmniLiveCoreKit and all buyer side related functions. |
|  `pod 'WeOmniLiveKit/Seller'` | WeOmniLiveCoreKit and all seller side related functions. |

### Step 3: Installation
```bash
pod install
```

### Step 4: `AppDelegate.swift`
#### Import the SDK
```swift
import WeOmniLiveCoreKit
```

#### Initiate the WeOmniLiveKit configuration and instance
```swift
let configuration = WLKConfiguration(clientId: String, clientSecret: String, projectId: String, environment: WLKConfiguration.Environment)
let instance = WeOmniLiveKit(configuration: WLKConfiguration)
```

_WeOmniLiveKit_ _supports multiple sources of data by separating instances. Each specific instance will be used in specific view components._

#### Notify WeOmniLiveKit on application startup in `application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool`
```swift
instance.application(application, didFinishLaunchingWithOptions: launchOptions)
```

#### Notify WeOmniLiveKit on application active `applicationDidBecomeActive(_ application: UIApplication)`
```swift
instance.applicationDidBecomeActive(application)
```

# WeOmniLiveKit for Buyer
## View Components
### WLKStreamingRoomListView
WeOmniLiveKit provided WLKStreamingRoomListView for displaying currently available streaming rooms as a single embedded view. This SDK-provided view also supports pull-to-refresh and load more features without any modification.
```swift
let streamingRoomListView = WLKStreamingRoomListView(wlk: WeOmniLiveKit, scrollDirection: UICollectionView.ScrollDirection, status: [WLKStreamingRoom.Status], numberOfItemsInPage: Int, customParameters: [String : Any])
```
> `wlk` - The SDK instance.
> `scrollDirection` - Scrolling direction for this view which supports both horizontal and vertical scrolling.
> `status` - Streaming rooms status to be fetched. See WLKStreamingRoom.Status
> `numberOfItemsInPage` - Number of rooms to be fetched from service.
> `customParameters` - User-provided custom reference parameters.

#### Delegation
This SDK-provided view currently supports 3 actions.  
##### `didSelectStreamingRoom`
##### `didPullToRefreshTriggered`
##### `didLoadMoreTriggered`

### WLKStreamingViewController
WeOmniLiveKit provided WLKStreamingViewController to be acts as whole player view packed with all required experiences.
```swift
let streamingViewController = WLKStreamingViewController(wlk: WeOmniLiveKit, streamingRoom: WLKStreamingRoom, overlayWebViewUrlString: String?, isAutoplayEnabled: Bool)
```
#### Delegation
This SDK-provided view currently supports an action.
#####  `didSelectUrl`

## Available API Services
WeOmniLiveKit also provided services for getting data from API directly.

### Room Service
#### Get WLKStreamingRoom list from API
```swift
instance.getStreamingRoomList(status: [WLKStreamingRoom.Status], size: Int, page: Int, customParameters: [String : Any], success: ([WLKStreamingRoom]) -> Void, failure: (WLKError) -> Void)
```

#### Get WLKStreamingRoom detail from API
```swift
instance.getStreamingRoom(id: String, success: (WLKStreamingRoom) -> Void, failure: (WLKError) -> Void)
```

# WeOmniLiveKit Example Project
To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Installation
WeOmniLiveKit is available through [CocoaPods](https://cocoapods.org). To install it, simply add the following line to your Podfile:
```ruby
pod 'WeOmniLiveKit'
```

# Author
Chayanon Ardkham, chayanon.ard@ascendcorp.com

# License
*WeOmniLiveKit* is available under the MIT license. See the LICENSE file for more info.
