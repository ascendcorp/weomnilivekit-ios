//
//  WLKBroadcastingProfile.swift
//  
//
//  Created by Chayanon Ardkham on 3/3/21.
//
//  swiftlint:disable identifier_name

import Foundation
import UIKit

public enum WLKBroadcastingProfile {
    case hd_1080p_30fps_5mbps
    case hd_720p_30fps_3mbps
    case sd_540p_30fps_2mbps
    case sd_360p_30fps_1mbps
    
    var width: Int {
        switch self {
        case .hd_1080p_30fps_5mbps:
            return 1920
        case .hd_720p_30fps_3mbps:
            return 1280
        case .sd_540p_30fps_2mbps:
            return 960
        case .sd_360p_30fps_1mbps:
            return 640
        }
    }
    
    var height: Int {
        switch self {
        case .hd_1080p_30fps_5mbps:
            return 1080
        case .hd_720p_30fps_3mbps:
            return 720
        case .sd_540p_30fps_2mbps:
            return 540
        case .sd_360p_30fps_1mbps:
            return 360
        }
    }
    
    var frameRate: Int {
        switch self {
        case .hd_1080p_30fps_5mbps:
            return 30
        case .hd_720p_30fps_3mbps:
            return 30
        case .sd_540p_30fps_2mbps:
            return 30
        case .sd_360p_30fps_1mbps:
            return 30
        }
    }
    
    var bitrate: Int {
        switch self {
        case .hd_1080p_30fps_5mbps:
            return 5000000
        case .hd_720p_30fps_3mbps:
            return 3000000
        case .sd_540p_30fps_2mbps:
            return 2000000
        case .sd_360p_30fps_1mbps:
            return 1000000
        }
    }
}
