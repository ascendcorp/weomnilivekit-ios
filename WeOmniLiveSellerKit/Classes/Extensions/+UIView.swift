//
//  +UIView.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 15/3/21.
//

import Foundation
import UIKit

extension UIView {
    
    @discardableResult
    func initFromNib<T: UIView>() -> T? {
        guard let contentView = Bundle.module.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as? T else {
            return nil
        }
        return contentView
    }
}
