//
//  +Bundle.swift
//  WeOmniLiveKit
//
//  Created by Chayanon Ardkham on 15/3/21.
//

import Foundation

extension Bundle {
    static var module: Bundle {
        return Bundle(for: WLKBroadcastingView.self.classForCoder())
    }
}
